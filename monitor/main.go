package main

import (
	"bitbucket.org/vithubati/jetstream/model"
	"encoding/json"
	"fmt"
	"log"
	"runtime"

	"github.com/nats-io/nats.go"
)

func main() {
	// Connect to NATS
	nc, _ := nats.Connect(nats.DefaultURL)
	js, err := nc.JetStream()
	if err != nil {
		log.Fatal(err)
	}
	// Create durable consumer monitor
	js.Subscribe("ORDERS.approved", func(msg *nats.Msg) {
		fmt.Println("msg.Sub.IsValid():=== ", msg.Sub.Type())
		err := msg.Ack()
		if err != nil {
			log.Printf("mError==========t:%s\n", err.Error())
			log.Fatal(err)
		}
		var order model.Order
		err = json.Unmarshal(msg.Data, &order)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("monitor service subscribes from subject:%s\n", msg.Subject)
		log.Printf("OrderID:%d, CustomerID: %s, Status:%s\n", order.OrderID, order.CustomerID, order.Status)
	}, nats.Durable("monitor"), nats.ManualAck())

	runtime.Goexit()

}
